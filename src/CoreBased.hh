#include <vector>
#include <functional>
#include "MiniSatExt.hh"
#include "auxiliary.hh"
#include "Lifter.hh"
#include "ToolConfig.hh"
#include "Rotatable.hh"
#include "BackboneInformation.hh"
#include "LitBitSet.hh"
using Minisat::MiniSatExt;
using Minisat::Var;
using Minisat::Lit;
using Minisat::vec;

namespace minibones {
    class CoreBased : public BackboneInformation {
    public: 
      CoreBased(ToolConfig& tool_configuration, 
                std::ostream& output, 
                Var max_id, const CNF& clauses);
      virtual ~CoreBased();
      bool initialize();// initialize, returns true iff the instance is SAT
      void run();// start the worker, initialize must be called first.
      virtual bool is_backbone(const Lit& literal) const;
      size_t  get_solver_calls() const { return solver_calls; }
      size_t  get_solver_time() const  { return solver_time; }

        private: 
      LitBitSet           might_be; 
      LitBitSet           must_be;  
      LitBitSet           visited;

      const ToolConfig&   tool_configuration;
      ostream&            output;
      const Var           max_id;
      const CNF&          clauses;
      CNF                 whiten_clauses;
      /** non whiten clauses with a given model */
      CNF                 non_whiten_clauses;
      vec<Lit>            modelLit;
      vec<lbool>          pre_model;
      bool                budget;
      /** accurate non-backbone, ordered by the layer of its generation */
      vec<Lit>            non_backbone;
      /** unqiue satisfiy literal with a given model, the satisfied literal in non whiten clause */
      LitBitSet           unique_satisfy_literals;
      LitBitSet           white_literals;
      /** outer vec stores lit, started from [1], inner lit stores clauses number, clauses[i] is stored as i, indicates the i+1 th clauses, since clauses is started from 1 */
      vector<vector<int>> pos_lit_in_clauses;
      vector<vector<int>> neg_lit_in_clauses;
      vec<Lit>            flip_literals;
      vec<Lit>            difference;
      bool                test_high_possible;

      LitBitSet           conjunction;
      Lit                 a;
      Lit                 b;
      Lit                 c;
      Lit                 next_a;
      Lit                 next_b;
      Lit                 next_c;

      size_t              solver_calls;// number of solver calls, for statistical purposes
      double              solver_time;// for statistical purposes
      double              previous_solver_time; // last SAT solving call time
      size_t              base_conflicts;// conflicts cnt of the first SAT testing
      size_t              previous_conflicts;// previous conflicts cnf of SAT testing
      size_t              base_time;//time to compute the first SAT testing
      float               previous_white_clauses_ratio = 0;

      double              conflict_ratio;// the ratio of availiable new conflicts


      Lifter              lifter;// used to reduce models via lifting
      Rotatable           rotatable_computer;// used to get rotatable variables


      MiniSatExt sat_solver;
      void process_model(const vec<lbool>& model);// calls pruning techniques and debones based of the model
      void process_pruned_model(const vec<lbool>& model);// debones everything not in the model
      size_t make_chunk(vec<Lit>& literals, int chunk_size);
      inline bool debone(Lit literal);// mark a literal as not a backbone
      inline void mark_backbone(Lit literal);// mark a literal as a backbone
      void test_backbones(LitBitSet& literals);
      inline bool run_sat_solver(const vec<Lit>& assumptions);
      inline lbool run_sat_solver_limited(const vec<Lit>& assumptions);
      inline bool run_sat_solver();

      void whiten(vec<Lit>& model);
      /** extend whiten literal by only flip one literal at each iteration*/
      void flip1();
      void compute_white_literals(vec<lbool> &model, vec<Lit> &lits);
      void extend(vec<Lit> & high_possible_backbone);

      void find_equal_relation();

      void get_max_coverage_ordered_literals(vector<int> &pos_literals, vector<int> &neg_literals);               // return a vec of literals, with the order of max coverage
      

  };
    
  inline bool CoreBased::debone(Lit literal) { 
    assert(!must_be.get(literal));
    return might_be.remove(literal);
  }

  inline void CoreBased::mark_backbone(Lit literal) { 
    // literal is the negation of backbone
    // find clauses has two literals, contians literal, the other literal is also backbone
    vector<int> neg_clauses = neg_lit_in_clauses[var(literal)];
    vector<int> pos_clauses = pos_lit_in_clauses[var(literal)];
    vector<int> bi_clauses;
    if(sign(literal)){
      bi_clauses = pos_clauses;
    } else {
      bi_clauses = neg_clauses;
    }
    for(int i = 0; i < bi_clauses.size(); ++i){
      LitSet lits = clauses[bi_clauses[i]];
      if(lits.size() > 2) {continue;}
      for(int j = 0; j < lits.size(); ++j){
        if(var(lits[j]) != var(literal)){
          // cout<<"implied bakcbone"<<endl;
          // cout<<(sign(lits[j]) ? "-" : "+")<<var(lits[j])<<" ";
          might_be.remove(lits[j]);
          might_be.remove(~lits[j]);
          must_be.add(lits[j]);
          sat_solver.addClause(lits[j]);
        }
      }
    }

    assert(might_be.get(literal));
    might_be.remove(literal);
    might_be.remove(~literal);
    if (must_be.add(literal)) {
      if (tool_configuration.get_backbone_insertion()) {
        sat_solver.addClause(literal);
      }
    }
  }

  bool CoreBased::run_sat_solver(const vec<Lit>& assumptions) {
    const auto t0 = read_cpu_time();
    const bool retv = sat_solver.solve(assumptions);
    // cout<<read_cpu_time() - t0<<endl;
    const auto t1 = read_cpu_time();
    solver_time+=(t1-t0);
    ++solver_calls;
    return retv;
  }

  lbool CoreBased::run_sat_solver_limited(const vec<Lit>& assumptions) {
    size_t time_threshold = base_time;
    const auto t0 = read_cpu_time();
    float conf_cnt = sat_solver.conflicts;
    if(conf_cnt > 0 && sat_solver.conflicts < base_conflicts * 100){
      sat_solver.setConfBudget(previous_conflicts*conflict_ratio);
    } else{
      sat_solver.budgetOff();
    }
    if(!budget){
      sat_solver.budgetOff();
    }
    const lbool retv = sat_solver.solveLimited(assumptions);
    // cout<<read_cpu_time() - t0<<endl;
    previous_solver_time = read_cpu_time() - t0;
    if(conflict_ratio > 0.000000001){
      if(read_cpu_time() - t0 > time_threshold){
        conflict_ratio = conflict_ratio * 0.001;
      } else {
        conflict_ratio = conflict_ratio * 0.99;
      }
    }
    const auto t1 = read_cpu_time();
    solver_time+=(t1-t0);
    ++solver_calls;
    return retv;
  }

  bool CoreBased::run_sat_solver() {
    const vec<Lit> assumptions;
    return run_sat_solver(assumptions);
  }

}

