#line 47 "BBRed.nw"
#ifndef BBRED_HH_7380
#define BBRED_HH_7380
#line 64 "BBRed.nw"
#include "types.hh"

#line 88 "BBRed.nw"
#include <unordered_map>
#include <vector>
using std::unordered_map;
using std::vector;

#line 254 "BBRed.nw"
#include "LitBitSet.hh"

#line 50 "BBRed.nw"
class BBRed {
public:
#line 71 "BBRed.nw"
BBRed(Var max_id, const CNF& cnf);
void run();
inline bool is_redundant(size_t clause_index) const;
inline size_t get_redundant_count() const;

#line 52 "BBRed.nw"
private:
#line 94 "BBRed.nw"
unordered_map<int, vector<size_t> > lit2indices;

#line 100 "BBRed.nw"
size_t bbcount;
bool inverted;
void invert(const CNF& cnf);

#line 130 "BBRed.nw"
const vector<size_t>& contain(const Lit l);
const vector<size_t> empty_vector;

#line 177 "BBRed.nw"
vector<Var> controls;
unordered_map<Var, size_t> control2index;

#line 185 "BBRed.nw"
const Var max_id;
Solver solver;

#line 212 "BBRed.nw"
vector<bool> redundant;
size_t redundant_count;

#line 359 "BBRed.nw"
void handle_backbone(
  LitBitSet& might_be, LitBitSet& must_be,
  Lit bb, const vec<Lit>& conflict);

#line 428 "BBRed.nw"
void initialize_might_be(LitBitSet& might_be);
void mark_redundant(size_t clause_index);
void build_core(const vec<Lit>& conflict, vector<bool>& core, Lit bb_lit);
void prune_might_be(LitBitSet& might_be, const vec<lbool>& model, Lit to_test);
void build_assumptions(Lit to_test, vec<Lit>& assumptions);
void fill_controls_and_sat(Var maxid, const CNF& cnf);
#line 53 "BBRed.nw"
};
#line 232 "BBRed.nw"
inline bool BBRed::is_redundant(size_t clause_index) const {
  return redundant[clause_index];
}

#line 237 "BBRed.nw"
inline size_t BBRed::get_redundant_count() const {
  return redundant_count;
}

#line 55 "BBRed.nw"
#endif

