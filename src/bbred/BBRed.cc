#line 58 "BBRed.nw"
#include "BBRed.hh"
#line 105 "BBRed.nw"
void BBRed::invert(const CNF& cnf) {
  assert(!inverted);
  for (size_t cl_index = 0; cl_index<cnf.size(); ++cl_index) {
    const LitSet& clause = cnf[cl_index];     
    FOR_EACH(itl, clause) {
      const Lit l = *itl;
      const int intl = Minisat::toInt(l);
      const auto i = lit2indices.find(intl);
      if (i==lit2indices.end()) {
        lit2indices[intl].push_back(cl_index);
      } else {
        i->second.push_back(cl_index);
      }
    }
  }
  inverted=true;
}

#line 134 "BBRed.nw"
const vector<size_t>& BBRed::contain(const Lit l) {
  assert(inverted);
  const int intl = Minisat::toInt(l);
  const auto i = lit2indices.find(intl);
  if (i==lit2indices.end()) return empty_vector;
  return i->second;
}

#line 189 "BBRed.nw"
void BBRed::fill_controls_and_sat(Var maxid, const CNF& cnf) {
  while (solver.nVars() <= max_id) solver.newVar();
  assert(solver.nVars()>max_id); // variable 0 is not used
  vec<Lit> lv;
  for (size_t cl_index = 0; cl_index<cnf.size(); ++cl_index) {
    const LitSet& clause = cnf[cl_index];
    const Var control_variable = solver.newVar();
    lv.clear();
    lv.push(~mkLit(control_variable));
    FOR_EACH(itl, clause) lv.push(*itl);
    solver.addClause_(lv);

    controls[cl_index] = control_variable;
    control2index[control_variable] = cl_index;
  }
}

#line 219 "BBRed.nw"
BBRed::BBRed(Var max_id, const CNF& cnf)
: bbcount(0)
, inverted(false)
, controls(cnf.size(), 0)
, max_id(max_id)
, redundant(cnf.size(), false)
, redundant_count(0)
{
  invert(cnf);
  fill_controls_and_sat(max_id, cnf);
}

#line 263 "BBRed.nw"
void BBRed::run() {
 LitBitSet might_be;
 LitBitSet must_be;
 initialize_might_be(might_be);

 vec<Lit> assumptions;
 while (might_be.size()) {
   const Lit to_test = *(might_be.begin());
   assumptions.clear();
   build_assumptions(to_test, assumptions);
   const bool isbb = !solver.solve(assumptions);
   if (isbb) handle_backbone(might_be, must_be, to_test, solver.conflict);
   else prune_might_be(might_be,solver.model,to_test);
 }
 std::cerr<<"BB count: "<<bbcount<<std::endl;
}

#line 291 "BBRed.nw"
void BBRed::initialize_might_be(LitBitSet& might_be) {
 vec<Lit> assumptions;
 FOR_EACH(ci,controls) assumptions.push(mkLit(*ci));
 const bool is_sat = solver.solve(assumptions);

 if (is_sat) {
   const auto& model = solver.model;
   const Var limit = std::min(model.size(),max_id+1);
   for (int i=1; i<limit; ++i) {
     const auto val = model[i];
     if (val==l_Undef) continue;
     might_be.add(val==l_True ? mkLit(i) : ~mkLit(i));
   }
 } else {
   for (Var i=1; i<=max_id; ++i) {
     might_be.add(mkLit(i));
     might_be.add(~mkLit(i));
   }
 }
}

#line 321 "BBRed.nw"
void BBRed::build_assumptions(Lit to_test, vec<Lit>& assumptions) {
  for (size_t cl_index=0; cl_index<controls.size(); ++cl_index) {
    if (!is_redundant(cl_index))
      assumptions.push(mkLit(controls[cl_index]));
  }
  assumptions.push(~to_test);
}

#line 335 "BBRed.nw"
void BBRed::prune_might_be(LitBitSet& might_be,
                           const vec<lbool>& model,
                           Lit to_test) {
  assert((var(to_test)>=model.size()) ||
         !sign(to_test) || model[var(to_test)]!=l_False);
  assert((var(to_test)>=model.size()) ||
         sign(to_test) || model[var(to_test)]!=l_True);

  const Var limit = std::min(model.size(),max_id+1);
  for (int i=1; i<limit; ++i) {
     const auto val = model[i];
     if (val!=l_True) might_be.remove(mkLit(i));
     if (val!=l_False) might_be.remove(~mkLit(i));
  }
}

#line 364 "BBRed.nw"
void BBRed::handle_backbone(
  LitBitSet& might_be, LitBitSet& must_be,
  Lit bb, const vec<Lit>& conflict) {
  assert(might_be.get(bb));
  assert(!must_be.get(bb));
  ++bbcount;
  might_be.remove(bb);
  must_be.add(bb);

  vector<bool> core(controls.size(),false);
  build_core(conflict, core, bb);

  const auto& bb_clauses = contain(bb);
  size_t old_redundant_count = redundant_count;
  FOR_EACH(cit,bb_clauses) {
    const size_t clause_index = *cit;
    if (!core[clause_index])
      mark_redundant(clause_index);
  }
  std::cerr<<"remove: "<<(redundant_count-old_redundant_count)<<std::endl;
}

#line 393 "BBRed.nw"
void BBRed::mark_redundant(size_t clause_index) {
  assert(clause_index<controls.size());
  if (redundant[clause_index]) return; //already known redundant
  ++redundant_count;
  redundant[clause_index] = true;
  solver.addClause(~mkLit(controls[clause_index]));
}


#line 409 "BBRed.nw"
void BBRed::build_core(const vec<Lit>& conflict,
                       vector<bool>& core,
                       Lit bb_lit) {
  assert(core.size()==controls.size());
  size_t c=0;
  for (int i=0; i<conflict.size(); ++i) {
    const Lit reason = conflict[i];
    if (bb_lit==reason) continue;
    assert(sign(reason));
    const auto clause_index = control2index.find(var(reason));
    assert(clause_index!=control2index.end());
    core[clause_index->second]=true;
    ++c;
  }
  std::cerr<<"csz: "<<c<<std::endl;
}


