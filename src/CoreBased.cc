#include "CoreBased.hh"
#include <iostream>
using namespace minibones;
using namespace std;

CoreBased::CoreBased(ToolConfig& ptool_configuration, ostream& poutput, 
   Var pmax_id, const CNF& pclauses)
: tool_configuration(ptool_configuration)
, output(poutput)
, max_id(pmax_id)
, clauses(pclauses)
, whiten_clauses(pclauses)
, non_whiten_clauses(pclauses)
, solver_calls(0)
, solver_time(0)
, lifter(clauses)
, rotatable_computer(clauses)
{ /* */ }

CoreBased::~CoreBased() { /* */ }

/**
Compute Whiten clause with a given model
*/
void CoreBased::whiten(vec<Lit>& model){
  unique_satisfy_literals.clear();
  whiten_clauses.clear();
  non_whiten_clauses.clear();
  int satisfied_literal_cnt = 0;
  int i = 0;
  FOR_EACH(ci,clauses) {
    satisfied_literal_cnt = 0;
    Lit unique_satisfy;
    FOR_EACH(li, *ci){
      if(sign(*li) != sign(model[var(*li)])){
        satisfied_literal_cnt++;
        unique_satisfy = *li;
      }
      if(sign(*li) == false){
        pos_lit_in_clauses[var(*li)].push_back(i);
      } else if(sign(*li) == true){
        neg_lit_in_clauses[var(*li)].push_back(i);
      }
    }
    if(satisfied_literal_cnt > 1){
      whiten_clauses.push_back(*ci);
    } else {
      // FOR_EACH(li, *ci){
        // cout<<(sign(*li) ? "-" : "+")<<var(*li)<<" ";
      // }
      non_whiten_clauses.push_back(*ci);
      unique_satisfy_literals.add(~unique_satisfy);
      // cout<<" uq " << (sign(unique_satisfy) ? "-" : "+")<<var(unique_satisfy)<< " ";
      // cout<<endl;

    }
    i++;
  }
  // cout<<"whiten cls size "<<whiten_clauses.size()<<endl;
  // cout<<"cls size "<<clauses.size()<<endl;
  // cout<<"whiten cls ratio "<<(float)whiten_clauses.size() / (float)clauses.size()<<endl;
  // cout<<"unique satisfy literals size "<<unique_satisfy_literals.size()<<endl;
  // cout<<"unique satisfy literals ratio "<<(float)unique_satisfy_literals.size() / (float)sat_solver.nVars()<<endl;
}

void CoreBased::flip1(){
  // find whiten variables in the first layer
  // layer by layer
  // random
  // chunk_size shrink to 1
  // free backbone
  // group unsat, test if it is sat by removing one of the learnt clause, according to the threshold conflict of previous model computing
  // group unsat, put learnt reason at the first position
  // all assumpiton, remove one learnt reason from each group, according to the threshold conflict of previous minisat

  // extend whiten literal to flip one literal in a unique satisfy clause

  CNF new_whiten_clauses;
  CNF new_non_whiten_clauses;
  FOR_EACH(ci,non_whiten_clauses){
    bool white = false;
    FOR_EACH(li,*ci){
      // cout<<(sign(*li) ? "-" : "+")<<var(*li)<<" ";
      // find a whiten literal in non whiten clauses
      if(!unique_satisfy_literals.get(~*li) && !unique_satisfy_literals.get(*li)){
        new_whiten_clauses.push_back(*ci);
        whiten_clauses.push_back(*ci);
        white = true;
        break;
      } 
    }
    if(white == false){
      new_non_whiten_clauses.push_back(*ci);
    }
    // cout<<endl;
  }
  // cout<<"new whiten clauses size "<<new_whiten_clauses.size()<<endl;
  // cout<<"new non whiten clauses size "<<new_non_whiten_clauses.size()<<endl;
  unique_satisfy_literals.clear();
  LitBitSet new_non_whiten_literals;
  FOR_EACH(ci, new_non_whiten_clauses){
    FOR_EACH(li, *ci){
      if(sign(*li) != sign(modelLit[var(*li)])){
        new_non_whiten_literals.add(~*li);
        unique_satisfy_literals.add(~*li);
        break;
      }
    }
  }
  // cout<<"new unique satisfy literals size "<<new_non_whiten_literals.size()<<endl;
}

/**
	compute whiten variables (approximation) according to the current non backbone
	triggered when there is no more rotatble literals
	until no update
	return the complement of whiten variaables (approximations)
*/
void CoreBased::extend(vec<Lit> & high_possibile_backbone){
	LitBitSet non_backbone_set;
	for(int i = 0; i < non_backbone.size(); ++i){
		non_backbone_set.add(non_backbone[i]);
		non_backbone_set.add(~non_backbone[i]);
	}
	// if a clause contains non_backbone, then this clause is in new_whiten_clauses
	CNF new_whiten_clauses;
	CNF new_non_white_clauses;
	FOR_EACH(ci,clauses){
		bool whiten = false;
		FOR_EACH(li,*ci){
			if(non_backbone_set.get(*li)){
				new_whiten_clauses.push_back(*ci);
				whiten = true;
				break;
			}
		}
		if(!whiten){
			new_non_white_clauses.push_back(*ci);
		}
	}
	LitBitSet new_non_white_literals;
	FOR_EACH(ci, new_non_white_clauses){
		FOR_EACH(li,*ci){
			if(sat_solver.model[var(*li)] == l_True){
				bool insert = new_non_white_literals.add(~*li);
			}
		}
	}
	float white_clauses_ratio = (float) new_whiten_clauses.size() / (float) clauses.size();
	// cout<<test_high_possible<<endl;
	if((white_clauses_ratio - previous_white_clauses_ratio) < 0.001 && !test_high_possible){
		test_high_possible = true;
		// cout<<"new non whiten literals ratio"<<(float)new_non_white_literals.size() / (float)sat_solver.nVars()<<endl;
		test_backbones(new_non_white_literals);
	}
	previous_white_clauses_ratio = white_clauses_ratio;


}

void CoreBased::compute_white_literals(vec<lbool> &model, vec<Lit> &white_literlas){
  LitBitSet white_clauses_of_one_model;
  // cout<<"white_literlas size "<<white_literlas.size()<<endl;
  for(int i = 0; i < white_literlas.size(); ++i){
    Lit l = white_literlas[i];
    if(sign(l)){
      for(int j = 0; j < pos_lit_in_clauses[var(l)].size(); ++j){
        int cls_id = pos_lit_in_clauses[var(l)][j];
        if(clauses[cls_id].size() == 2 ) continue;
        white_clauses_of_one_model.add(mkLit(cls_id,true));
      }
    } else {
      for(int j = 0; j < neg_lit_in_clauses[var(l)].size(); ++j){
        int cls_id = neg_lit_in_clauses[var(l)][j];
        if(clauses[cls_id].size() == 2) continue;
        white_clauses_of_one_model.add(mkLit(cls_id,true));
      }
    }
  }
  // cout<<"new white clauses "<<white_clauses_of_one_model.size()<<endl;
  // if a literal only satisfies in this clauses, is a non-backbone
  LitBitSet not_non_backbone;
  int cls_id = 0;
  FOR_EACH(ci,clauses){
    if(white_clauses_of_one_model.get(mkLit(cls_id,true))){
      cls_id++;
      continue;
    } else {
      cls_id++;
      FOR_EACH(li,*ci){
        if((model[var(*li)] == l_True) != sign(*li)){
          not_non_backbone.add(*li);
        }
      }
    }
  }
  // LitBitSet non_backbone;
  int count = 0;
  // cout<<"model ";
  for(int i = 1; i < sat_solver.nVars(); ++i){
    // cout<<((sat_solver.model[i] == l_True) ? "+" : "-")<<i<<" ";
    if(!not_non_backbone.get(mkLit(i,true)) && !not_non_backbone.get(mkLit(i,false))){
      non_backbone.push(mkLit(i));
      if(might_be.remove(mkLit(i,true)) || might_be.remove(mkLit(i,false))){
        count++;
      }
    }
  }
  // cout<<endl;
  // cout<<"not non backbone size "<<not_non_backbone.size()<<endl;
  // cout<<"prune might_be size "<<count<<endl;
  // cout<<"non-backbone size "<<non_backbone.size()<<endl;
  vec<Lit> test;
  if(!test_high_possible){
  	// extend(test);
	}
}

void CoreBased::find_equal_relation(){
  // read first 6 clauses
  LitBitSet important;
  bool pre_implied = false;
  LitBitSet implication;
  for(int i = 0; i < clauses.size(); ){
    Lit a = mkLit(0, true), b = mkLit(0, true),c = mkLit(0,true);
    Lit next_a = mkLit(0, true), next_b = mkLit(0, true), next_c = mkLit(0, true);
    Lit pre_next_c = mkLit(0,true);
    LitBitSet first_three_clauses;
    LitBitSet second_three_literals;
    for(int j = 0; j < 3; j++){
      // we only find clauses with 2 or 3 length
      if(clauses[i].size() != 3 && clauses[i].size() != 2){
        i = i + 3;
        break;
      } else{
        for(int k = 0; k < clauses[i].size(); ++k){
          first_three_clauses.add(clauses[i][k]);
        }
      }
      i++;
    }
    for(int j = 0; j < 3; j++){
      if(clauses[i].size() != 2 && clauses[i].size() != 3){
        i = i + 3;
        break;
      } else{
        for(int k = 0; k < clauses[i].size(); ++k){
          if(first_three_clauses.get(clauses[i][k]) > 0 && var(c) == 0){
            c = ~clauses[i][k];
            for(int m = 0; m < clauses[i - j - 1].size(); ++m){
              // cout<<sign(clauses[i - j - 1][m])<<" "<<var(clauses[i - j - 1][m])<<endl;
              if(var(clauses[i - j - 1][m]) != var(c)){
                if(var(a) != 0 ){
                  b = clauses[i - j - 1][m];
                } else{
                  a = clauses[i - j - 1][m];
                }
              }
            }
            next_a = c;
            // cout<<"a : "<<sign(a)<<var(a)<<endl;
            // cout<<"b : "<<sign(b)<<var(b)<<endl;
            // cout<<"c : "<<sign(c)<<var(c)<<endl;
          }
          second_three_literals.add(clauses[i][k]);
        }
      }
      i++;
    }
    for(int m = 0; m < clauses[i - 3].size(); ++m){
      if(var(clauses[i - 3][m]) == var(next_a)){
        if(m == 0){
          next_c = clauses[i - 3][1];
        } else {
          next_c = clauses[i - 3][0];
        }
      }     
    }
    for(int m = 0; m < clauses[i - 2].size(); ++m){
      if(var(clauses[i - 2][m]) != var(next_c)){
        next_b = ~clauses[i - 2][m];
      }     
    }
    // cout<<"next a : "<<sign(next_a)<<var(next_a)<<endl;
    // cout<<"next b : "<<sign(next_b)<<var(next_b)<<endl;
    // cout<<"next c : "<<sign(next_c)<<var(next_c)<<endl;
    //test a,b,c and next_a, next_b, next_c
    bool implied = true;
    for(int m = 0; m < clauses[i - 6].size(); ++m){
      if(clauses[i - 6][m] != c && clauses[i - 6][m] != ~a && clauses[i - 6][m] != ~b){
        implied = false;
      }
    }
    for(int m = 0; m < clauses[i - 5].size(); ++m){
      if(clauses[i - 5][m] != c && clauses[i - 5][m] != ~a && clauses[i - 5][m] != ~b){
        implied = false;
      }
    }
    for(int m = 0; m < clauses[i - 4].size(); ++m){
      if(clauses[i - 4][m] != ~c && clauses[i - 4][m] != a && clauses[i - 4][m] != b){
        implied = false;
      }
    }
    for(int m = 0; m < clauses[i - 3].size(); ++m){
      if(clauses[i - 3][m] != next_c && clauses[i - 3][m] != ~next_a && clauses[i - 3][m] != ~next_b){
        implied = false;
      }
    }
    for(int m = 0; m < clauses[i - 2].size(); ++m){
      if(clauses[i - 2][m] != next_c && clauses[i - 2][m] != ~next_a && clauses[i - 2][m] != ~next_b){
        implied = false;
      }
    }
    for(int m = 0; m < clauses[i - 1].size(); ++m){
      if(clauses[i - 1][m] != ~next_c && clauses[i - 1][m] != next_a && clauses[i - 1][m] != next_b){
        implied = false;
      }
    }
    pre_next_c = next_c;
    // cout<<"implied : "<<implied<<endl;
    if (implied){
      implication.add(a);
      implication.add(b);
      implication.add(c);
      implication.add(next_a);
      implication.add(next_b);
      implication.add(next_c);
    } else {
      //test if next_c is backbone
      bool next_c_is_backbone = false;
      vec<Lit> test_next_c;
      test_next_c.clear();
      // cout<<"implication size"<<implication.size()<<endl;
      if(var(pre_next_c) != 0  && pre_implied && implication.size() > 30 ){
        // cout<<"pre next c "<<(sign(pre_next_c) ? "+" : "-")<<var(pre_next_c)<<endl;
        test_next_c.push(~pre_next_c);
        next_c_is_backbone = run_sat_solver(test_next_c);
        // cout<<"next is backbone"<<!next_c_is_backbone<<endl;
        if(!test_next_c){
          cout<<"implied information"<<endl;
        }
	process_model(sat_solver.model);
      }
      implication.clear();
    }
    pre_implied = implied;
  }
}

void CoreBased::get_max_coverage_ordered_literals (vector<int> &pos_lit, vector<int> &neg_lit) {
  // compute coverage of each literals in clauses
  int maximal_coverage = 0;
  pos_lit.push_back(0);
  neg_lit.push_back(0);
  for(int i = 1; i <= max_id; ++i){
    pos_lit.push_back(0);
    neg_lit.push_back(0);
  }
  FOR_EACH(ci, clauses) {
    FOR_EACH(li, *ci){
      const Lit l = *li;
      if(sign(l)){
        pos_lit[var(l)]++;
        if(pos_lit[var(l)] > maximal_coverage){
          maximal_coverage = pos_lit[var(l)];
        }
      } else {
        neg_lit[var(l)]++;
        if(neg_lit[var(l)] > maximal_coverage){
          maximal_coverage = neg_lit[var(l)];
        }
      }
    }
  }

  vector<vector<int>> coverage_number;        // specified literals according to the coverage number, 4[1,2,3] meams the coverage of literal 1,2,3 are 4 times
  vector<int> tmp;
  tmp.push_back(0);
  coverage_number.push_back(tmp);
  for(int i = 1; i <= maximal_coverage; ++i){
    coverage_number.push_back(tmp);
  }
  for(int i = 1; i <= max_id; ++i){
    coverage_number[pos_lit[i]].push_back(i);
    coverage_number[neg_lit[i]].push_back(-i);
  }

  cout<<"coverage ordered literals ";
  for(int i = coverage_number.size() ; i > 0; --i){
    cout<<"coverage is "<<i<<" ";
    for(int j = 1; j < coverage_number[i].size(); ++j){
      cout<<coverage_number[i][j]<<" ";
    }
    cout<<endl;
  }
  cout<<endl;

}

bool CoreBased::initialize() {
  //initialize the solver
  test_high_possible = false;
  whiten_clauses.clear();
  non_whiten_clauses.clear();
  sat_solver.new_variables(max_id);
  vec<Lit> ls;
  FOR_EACH(ci, clauses) {
    ls.clear ();
    FOR_EACH(li, *ci) {
      const Lit l = *li;
      assert (var(l) <= max_id);
      ls.push(l);
      // .cnf 34 0, l=(-34)
    }
    sat_solver.addClause(ls);
  }

  // get the first model
  auto t1 = read_cpu_time();
  // v2.0 give a largest assignment that doesn't fasify any clause
  // don't need to cover all clause
  // generate all 1, all 0, max coverage, randomly
  vector<int> pos_lit;
  vector<int> neg_lit;
  get_max_coverage_ordered_literals(pos_lit, neg_lit);

  const bool result = run_sat_solver();
  cout<<"first model computing time"<<read_cpu_time() - t1<<endl;
  previous_solver_time = read_cpu_time() - t1;
  base_time = previous_solver_time;
  base_conflicts = sat_solver.conflicts;
  if (!result) {// the whole instance is not satisfiable
    return false;
  }

  const vec<lbool>& model = sat_solver.model;
  sat_solver.model.copyTo(pre_model);
  // cout<<"model ";
  const Var max_model_var = std::min(model.size()-1,max_id);
  modelLit.push(mkLit(1,true));
  for (Var variable = 1; variable <= max_model_var; ++variable) {
      const lbool value = model[variable];
      if (value != l_Undef) {
        const Lit l = mkLit(variable, value==l_True);
        // cout<<(sign(l) ? "+" : "-")<<var(l)<<" ";
        modelLit.push(l);
      }
  }
  // cout<<endl;

  // initialize pos_lit_in_clauses and neg_lit_in_clauses
  vector<int> empty;
  for(int i = 0; i < sat_solver.nVars(); ++i){
    pos_lit_in_clauses.push_back(empty);
    neg_lit_in_clauses.push_back(empty);
  }

  // initialize whiten algorithm using the given model
  find_equal_relation();
  whiten(modelLit);
  flip1();
  for(int i = 0; i < modelLit.size(); ++i){
    // visited.add(~modelLit[i]);
    if(!unique_satisfy_literals.get(modelLit[i])) continue;
    might_be.add(~modelLit[i]);
  }
  process_model(sat_solver.model);

  return true;
}


void CoreBased::run() {
  LitBitSet relaxed; // literals removed from assumptions
  LitBitSet first_uc;// store the first uc in every iteration
  vec<Lit>  assumptions; // assumptions passed onto the SAT solver
  vec<Lit>  old_assumptions; // assumptions in prev.\ iteration of inner loop
  int chunk = 10;
  while (might_be.size()) {
    if(chunk < 200){
      chunk = chunk * 2;
    }
    size_t chunk_size = make_chunk(old_assumptions, chunk);
    // cout<<"chunk start with"<<var(old_assumptions[0])<<endl;
    // cout<<"old assumption size"<<old_assumptions.size()<<endl;
    
    relaxed.clear();
    // double initial_conflict_ratio = 0.0001;
    while (true) {
      // budget = true;
      // conflict_ratio = 1;
      // previous_conflicts = base_conflicts;
  
      assumptions.clear();
      for (int i=0; i<old_assumptions.size(); ++i) {
        const Lit l = old_assumptions[i];
        if (relaxed.get(l)) continue;
        assumptions.push(l);
      }
      if(assumptions.size() == 0) break;

      // cout<<"uc=1 time ";
      auto t1 = read_cpu_time();
      // const lbool flipped = run_sat_solver_limited(assumptions);
      const bool flipped = run_sat_solver(assumptions);
      // cout<<read_cpu_time()-t1<<endl;
      // cout<<"assumptions ";
      for(int i = 0; i < assumptions.size(); ++i){
        // cout<<(sign(assumptions[i]) ? "+" : "-")<<var(assumptions[i])<<" ";
      }
      // cout<<endl;
      if (flipped == true) { 
        difference.clear();
      // cerr<<"SAT"<<endl;
        // cout<<"save sat count "<<assumptions.size()<<endl;
        process_model(sat_solver.model);
        // model enumeration
        for(int i = 1; i < sat_solver.nVars(); ++i){
          if(pre_model[i] != sat_solver.model[i]){
            might_be.remove(mkLit(i,true));
            might_be.remove(mkLit(i,false));
            // if a literal only satisfies in clauses, such that contains the difference literals
            // this literal is a non-backbone
            // cout<<"model difference "<<i<<endl;
            difference.push(mkLit(i));
          }
        }
        // cout<<"difference size "<<difference.size()<<endl;
        compute_white_literals(sat_solver.model, difference);
        // cout<<"current might_be size "<<might_be.size()<<endl;
        sat_solver.model.copyTo(pre_model);
        // modelLit.copyTo(pre_modelLit);
        whiten(modelLit);
        break; // we are done with the chunk
      } else if(flipped == false){ 
        const auto& conflict = sat_solver.conflict;
        // cerr<<"RX:"<<relaxed.size()<<" MBS: "<<might_be.size()<<" CS: "<<conflict.size()<<endl;
        if (sat_solver.conflict.size()==1) { // A unit conflict means backbone. 
          mark_backbone(conflict[0]); 
          // visited.remove(conflict[0]);
          // visited.remove(~conflict[0]);
          //cout<<(sign(conflict[0]) ? "+" : "-" )<<var(conflict[0])<<endl;
        }
        // cout<<"uc";
        vec<Lit> learnt;
        for (int i=0; i<conflict.size(); ++i) {
          // cout<<(sign(conflict[i]) ? "+" : "-" )<<var(conflict[i])<<" ";
          relaxed.add(~conflict[i]);
          // visited.remove(conflict[i]);
          // visited.remove(~conflict[i]);
          learnt.push(conflict[i]);
        }
        // cout<<endl;
        assert(relaxed.size()<=chunk_size);
        if(conflict.size() > 1){
          first_uc.clear();
          first_uc.add(~conflict[0]);
          sat_solver.addClause(learnt);
          learnt.clear();
          auto t = read_cpu_time();
          test_backbones(first_uc);
          // cout<<"test first uc time "<<read_cpu_time()-t<<endl;
        }
      } 
      if (relaxed.size()>=chunk_size) {
        // cout<<"relaxed size "<<relaxed.size()<<endl;
          // budget = false;
          // int count = 0;
          // size_t previous_size = 0;
          // int out_budget = 0;
          // while(relaxed.size()){
            // previous_size = relaxed.size();
            // conflict_ratio = initial_conflict_ratio;
            // for(int i = 0; i < count; ++i){
              // conflict_ratio = conflict_ratio * 100;
            // }
        test_backbones(relaxed);
            // count++;
            // if(relaxed.size() == previous_size){
              // out_budget++;
              // if(out_budget > 10){
                // conflict_ratio = conflict_ratio * 10;
                // out_budget = 0;
              // }
            // }
          // }
          break; // we are done with the chunk
        }
      old_assumptions.clear();
      assumptions.copyTo(old_assumptions);
    }
  }
}

void CoreBased::test_backbones(LitBitSet& negliterals) {
  previous_conflicts = sat_solver.conflicts;
  vec<Lit> assumptions(1);
  FOR_EACH (li, negliterals) {
    const Lit negl=*li;
    const Lit    l=~negl;
    if (!might_be.get(l)) {
      negliterals.remove(negl);
      continue;
    }
    assumptions[0]=negl;
    // cout<<"test backbone literal "<<var(l)<<" time "; 
    const bool flipped = run_sat_solver(assumptions);
    if (flipped == true) {
      difference.clear();
      process_model(sat_solver.model);
      // int count = 0;
      for(int i = 0; i < sat_solver.nVars(); ++i){
        if(pre_model[i] != sat_solver.model[i]){
          difference.push(mkLit(i,true));
        }
      }
      // cout<<"remain might be in test backbone "<<might_be.size()<<endl;
      // cout<<"model difference in test backbone "<<count<<endl;
      negliterals.remove(negl);
      sat_solver.model.copyTo(pre_model);
    }
    else if(flipped == false) {
      mark_backbone(l);
      negliterals.remove(negl);
    } else {
      // cout<<"beyond conflict limitation"<<endl;
    }
  }
}

void CoreBased::process_model(const vec<lbool>& model) {
  if (tool_configuration.get_scdc_pruning()) {
    vec<lbool> rmodel;
    vec<Lit> assumptions;
    lifter.reduce(assumptions, model, rmodel);
    assert(lifter.is_model(rmodel));
    process_pruned_model(rmodel);
  } else if (tool_configuration.get_rotatable_pruning ()) {
    VarSet pruned;
    rotatable_computer.get_rotatables(model, pruned);
    FOR_EACH (vi, pruned) {
      const Lit pl = mkLit(*vi);
      const Lit nl = ~pl;
      debone(pl);
      debone(nl);
    }
    process_pruned_model(model);
  } else {
    process_pruned_model(model);
  }
}

void CoreBased::process_pruned_model(const vec<lbool>& model) {
  for (Var variable = 1; variable < model.size(); ++variable) {
    const lbool value = model[variable];
    const Lit   pos_lit = mkLit(variable);
    const Lit   neg_lit = ~mkLit(variable);
    if (value != l_True) might_be.remove(pos_lit);
    if (value != l_False) might_be.remove(neg_lit);
  }

  for (Var variable=std::max(model.size(),1); variable<=max_id; ++variable) {
    const Lit pos_lit = mkLit(variable);
    might_be.remove( pos_lit);
    might_be.remove(~pos_lit);
  }
}

size_t CoreBased::make_chunk(vec<Lit>& literals, int chunk) {
  // cout<<"chunk size"<<chunk<<endl;
  int might_be_size = might_be.size();
  // cout<<"might_be size"<<might_be_size<<endl;
  int real_chunk_size = (int)std::min(chunk, might_be_size); 
  // cout<<"real chunk size"<<real_chunk_size<<endl;
  literals.clear();
  int count = 0;
  FOR_EACH (li, might_be) {
    count++;
    if(count < 0) continue;
    if (literals.size() >= real_chunk_size) break;
    const Lit lit = *li;
    literals.push(~lit);
  }
  return literals.size();
}

bool CoreBased::is_backbone(const Lit& literal) const 
{ return must_be.get(literal); }


